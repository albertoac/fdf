#******************************************************************************#
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: aacuna <marvin@42.fr>                      +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2015/12/10 14:17:09 by aacuna            #+#    #+#              #
#    Updated: 2015/12/20 16:24:23 by aacuna           ###   ########.fr        #
#                                                                              #
#******************************************************************************#

NAME = fdf

SRCS = main.c			\
	   geometry.c		\
	   pixel.c			\
	   text_file.c 		\

FRAMEWORK = -framework OpenGL -framework AppKit

CFLAGS = -Wall -Wextra -Werror -I libft/includes/ -I minilibx_macos

LIBRARY = libft/libft.a minilibx_macos/libmlx.a

OBJS = $(SRCS:.c=.o)

$(NAME)	:	$(OBJS)
			make -C libft/
			make -C minilibx_macos
			gcc $(CFLAGS) $(FRAMEWORK) -o $(NAME) $(OBJS) $(LIBRARY)

all		:	$(NAME)

clean	:
			rm -rf $(OBJS)

fclean	:	clean
			rm -rf $(NAME)

re		:	fclean all
