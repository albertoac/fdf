# FDF #

FDF will only work on Mac computers.

### Installation ###

With the terminal, enter in the folder you just downloaded and type
```
#!shell

make

```

If everything is ok, the project will be compiled and a new file called "fdf" will be created.

### Usage ###

To execute the program you just need to run

```
#!shell

./fdf test_maps/42.fdf
```
 You can replace the file for any other fdf file. There are a lot of examples in the folder test_maps

![Screen Shot 2016-09-02 at 12.10.55 PM.png](https://bitbucket.org/repo/yLenpX/images/1190077400-Screen%20Shot%202016-09-02%20at%2012.10.55%20PM.png)