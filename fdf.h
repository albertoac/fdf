/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fdf.h                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aacuna <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/17 10:38:46 by aacuna            #+#    #+#             */
/*   Updated: 2015/12/21 18:07:38 by aacuna           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FDF_H
# define FDF_H
# define WIDTH 1520
# define HEIGHT 1020
# define MAX_HEIGHT 40

# include <fcntl.h>
# include <mlx.h>
# include <unistd.h>
# include <stdlib.h>
# include <math.h>
# include "libft.h"

typedef struct	s_enf
{
	void *mlx;
	void *win;
}				t_env;

typedef struct	s_point
{
	int x;
	int y;
	int	color;
	int height;
}				t_point;

int				get_width(char *file);
int				isometric_y(t_point p, int offset, float size, int height);
int				isometric_x(t_point p, int offset, float size);
double			to_radians(int x);
char			**get_next_line_tab(int fd);
int				get_max_value(char *file);
void			print_isometric(t_env env, char *file, int max);
int				calculate_color(float h, int max);
void			join_points(t_point p1, t_point p2, t_env env);
char			*get_value_for_pos(char **tab, int pos);

#endif
