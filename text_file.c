/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   text_file.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aacuna <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/17 10:46:48 by aacuna            #+#    #+#             */
/*   Updated: 2015/12/21 18:08:02 by aacuna           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

int		get_width(char *file)
{
	int		fd;
	int		i;
	char	**tab;

	fd = open(file, O_RDONLY);
	tab = get_next_line_tab(fd);
	i = 0;
	while (tab[i] != '\0')
		i++;
	close(fd);
	return (i);
}

int		tab_length(char **tab)
{
	int i;

	i = 0;
	if (tab)
		while (tab[i])
			i++;
	return (i);
}

char	*get_value_for_pos(char **tab, int pos)
{
	if (pos >= tab_length(tab))
		return (NULL);
	else
		return (tab[pos]);
}

int		get_max_value(char *file)
{
	int		fd;
	int		max;
	char	**tab;
	int		i;

	fd = open(file, O_RDONLY);
	max = 1;
	while ((tab = get_next_line_tab(fd)))
	{
		i = 0;
		while (tab[i])
		{
			if (ft_atoi(tab[i]) > max)
				max = ft_atoi(tab[i]);
			i++;
		}
	}
	return (max);
}

char	**get_next_line_tab(int fd)
{
	int		n;
	char	**result;
	char	*line;

	n = get_next_line(fd, &line);
	if (n < 0)
	{
		ft_putstr("ERROR: there was an error in the reading\n");
		exit(1);
	}
	if (n == 0)
		return (NULL);
	result = ft_strsplit(line, ' ');
	return (result);
}
