/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pixel.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aacuna <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/21 16:21:48 by aacuna            #+#    #+#             */
/*   Updated: 2015/12/21 18:07:00 by aacuna           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

t_point	calc_point(t_point p, char *h, float size, int max)
{
	t_point result;
	int		offset;
	float	height;
	float	real_height;

	offset = WIDTH / 2;
	height = ft_atoi(h);
	result.x = isometric_x(p, offset, size);
	real_height = MAX_HEIGHT * (height / max);
	result.y = isometric_y(p, 100, size, real_height);
	result.color = calculate_color(height, max);
	return (result);
}

void	print_pixel(t_env env, t_point p)
{
	mlx_pixel_put(env.mlx, env.win, p.x, p.y, p.color);
}

char	**loop_aux(t_point *p, char ***tab, char **next_tab)
{
	p->y++;
	free(*tab);
	return (next_tab);
}

void	print_loop(float sqr, int fd, t_env env, int max)
{
	t_point	p;
	char	**next_tab;
	char	**tab;
	t_point	coord;

	p.y = 0;
	tab = get_next_line_tab(fd);
	while (tab)
	{
		next_tab = get_next_line_tab(fd);
		p.x = 0;
		while (tab[p.x])
		{
			coord = calc_point(p, tab[p.x], sqr, max);
			print_pixel(env, coord);
			p.y++;
			if (next_tab && get_value_for_pos(next_tab, p.x))
				join_points(coord, calc_point(p, next_tab[p.x], sqr, max), env);
			p.y--;
			p.x++;
			if (tab[p.x])
				join_points(coord, calc_point(p, tab[p.x], sqr, max), env);
		}
		tab = loop_aux(&p, &tab, next_tab);
	}
}

void	print_isometric(t_env env, char *file, int max)
{
	int		fd;
	float	sqr;

	sqr = (WIDTH / (2 * cos(to_radians(30)))) / get_width(file);
	if (sqr > 50)
		sqr = 50;
	fd = open(file, O_RDONLY);
	print_loop(sqr, fd, env, max);
	close(fd);
}
