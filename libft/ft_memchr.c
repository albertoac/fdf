/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memchr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aacuna <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/24 10:18:06 by aacuna            #+#    #+#             */
/*   Updated: 2015/12/02 10:08:37 by aacuna           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memchr(const void *s, int c, size_t n)
{
	unsigned char elem;
	unsigned char *tab;

	elem = (unsigned char)c;
	tab = (unsigned char*)s;
	if (n == 0)
		return (NULL);
	while ((*tab != elem) && (n > 0))
	{
		tab++;
		n--;
	}
	if (n > 0)
		return (tab);
	else
		return (NULL);
}
