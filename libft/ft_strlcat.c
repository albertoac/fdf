/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlcat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aacuna <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/24 14:52:02 by aacuna            #+#    #+#             */
/*   Updated: 2015/12/02 10:12:47 by aacuna           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

size_t	ft_strlcat(char *dst, const char *src, size_t size)
{
	size_t	src_length;
	size_t	dst_length;
	int		free_bytes;

	src_length = ft_strlen(src);
	dst_length = ft_strlen(dst);
	free_bytes = size - dst_length - 1;
	if (free_bytes < 0)
		free_bytes = 0;
	ft_strncat(dst, src, free_bytes);
	if (dst_length > size)
		dst_length = size;
	return (src_length + dst_length);
}
