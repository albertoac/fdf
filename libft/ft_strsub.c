/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsub.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aacuna <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/25 15:57:10 by aacuna            #+#    #+#             */
/*   Updated: 2015/11/25 16:19:54 by aacuna           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>

char	*ft_strsub(char const *s, unsigned int start, size_t len)
{
	char	*tab;
	int		i;

	tab = (char*)malloc((len + 1) * sizeof(*tab));
	if (tab == NULL)
		return (NULL);
	i = 0;
	while (len > 0)
	{
		tab[i] = s[i + start];
		i++;
		len--;
	}
	tab[i] = '\0';
	return (tab);
}
