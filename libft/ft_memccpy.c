/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memccpy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aacuna <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/23 16:47:54 by aacuna            #+#    #+#             */
/*   Updated: 2015/11/23 18:51:30 by aacuna           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memccpy(void *dst, const void *src, int c, size_t n)
{
	unsigned char *pdst;
	unsigned char *psrc;
	unsigned char val;

	pdst = (unsigned char*)dst;
	psrc = (unsigned char*)src;
	val = (unsigned char)c;
	while ((n > 0) && (*psrc != val))
	{
		*pdst = *psrc;
		n--;
		pdst++;
		psrc++;
	}
	if (n != 0)
	{
		*pdst = *psrc;
		return (pdst + 1);
	}
	else
		return (NULL);
}
