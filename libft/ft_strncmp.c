/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncmp.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aacuna <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/24 17:45:22 by aacuna            #+#    #+#             */
/*   Updated: 2015/12/01 19:07:36 by aacuna           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int	ft_strncmp(const char *s1, const char *s2, size_t n)
{
	unsigned char *c1;
	unsigned char *c2;

	c1 = (unsigned char*)s1;
	c2 = (unsigned char*)s2;
	while ((*c1 != '\0') && ((*c1 - *c2) == 0) && (n > 0))
	{
		c1++;
		c2++;
		n--;
	}
	if (n == 0)
		return (0);
	else
		return (*c1 - *c2);
}
