/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aacuna <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/14 16:05:32 by aacuna            #+#    #+#             */
/*   Updated: 2015/12/15 14:33:10 by aacuna           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "get_next_line.h"

static int	next_cut(char *str)
{
	int i;

	i = 0;
	while (str[i] && (str[i] != '\n'))
		i++;
	return (i);
}

int			add_line(char **line, char *buf)
{
	char	*tmp;
	char	*tmp2;
	int		stop;

	tmp = ft_strsub(buf, 0, next_cut(buf));
	tmp2 = *line;
	*line = ft_strjoin(*line, tmp);
	free(tmp2);
	stop = (buf[next_cut(buf)] == '\n');
	if (buf[next_cut(buf)] == '\n')
		ft_memmove(buf, buf + next_cut(buf) + 1, BUFF_SIZE - next_cut(buf));
	else
		ft_memmove(buf, buf + next_cut(buf), BUFF_SIZE - next_cut(buf) + 1);
	bzero(buf + ft_strlen(buf), BUFF_SIZE - ft_strlen(buf));
	free(tmp);
	return (stop);
}

int			get_next_line(int const fd, char **line)
{
	static char	*buf = NULL;
	int			len;
	int			stop;

	if (line == NULL)
		return (-1);
	*line = NULL;
	if (buf == NULL)
		buf = ft_strnew(BUFF_SIZE);
	stop = 0;
	while (!stop)
	{
		if (buf[0] == '\0')
		{
			len = read(fd, buf, BUFF_SIZE);
			if (len < 0)
				return (-1);
			else if (len == 0)
				return (*line != NULL);
		}
		stop = add_line(line, buf);
	}
	return (1);
}
