/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstnew.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aacuna <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/26 15:56:53 by aacuna            #+#    #+#             */
/*   Updated: 2015/12/01 18:54:52 by aacuna           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>

t_list	*ft_lstnew(void const *content, size_t content_size)
{
	t_list	*new_list;
	void	*cont;

	new_list = (t_list*)malloc(sizeof(*new_list));
	if (new_list == NULL)
		return (NULL);
	cont = malloc(content_size * sizeof(*cont));
	if ((cont == NULL) || (content == NULL))
	{
		content_size = 0;
		new_list->content = NULL;
	}
	else
		new_list->content = ft_memcpy(cont, content, content_size);
	new_list->content_size = content_size;
	new_list->next = NULL;
	return (new_list);
}
