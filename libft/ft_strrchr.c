/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strrchr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aacuna <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/24 16:08:31 by aacuna            #+#    #+#             */
/*   Updated: 2015/11/24 16:17:03 by aacuna           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strrchr(const char *s, int c)
{
	char *str;
	char *found;

	found = NULL;
	str = (char*)s;
	while (*str != '\0')
	{
		if (*str == c)
			found = str;
		str++;
	}
	if (*str == c)
		found = str;
	return (found);
}
