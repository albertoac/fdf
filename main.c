/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aacuna <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/16 12:29:03 by aacuna            #+#    #+#             */
/*   Updated: 2015/12/21 14:57:43 by aacuna           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

int		calculate_gradient(t_point p1, t_point p2, float percent)
{
	int r;
	int g;
	int b;

	r = (p2.color / (256 * 256));
	g = ((p2.color / 256) % 256);
	b = (p2.color % 256);
	r = (r - (p1.color / (256 * 256))) * percent;
	g = (g - ((p1.color / 256) % 256)) * percent;
	b = (b - (p1.color % 256)) * percent;
	r = r + (p1.color / (256 * 256));
	g = g + ((p1.color / 256) % 256);
	b = b + (p1.color % 256);
	return ((r * 256 * 256) + (g * 256) + b);
}

void	join_points(t_point p1, t_point p2, t_env env)
{
	t_point tmp;
	float	diff;

	if (p1.x > p2.x)
	{
		tmp = p1;
		p1 = p2;
		p2 = tmp;
	}
	tmp = p1;
	if ((p1.x - p2.x) != 0)
	{
		diff = (p1.y - p2.y) / (float)(p1.x - p2.x);
		while (tmp.x < p2.x)
		{
			tmp.x++;
			tmp.y = diff * (tmp.x - p1.x) + p1.y;
			tmp.color = calculate_gradient(p1, p2, (p1.x - tmp.x)
											/ (float)(p1.x - p2.x));
			mlx_pixel_put(env.mlx, env.win, tmp.x, tmp.y, tmp.color);
		}
	}
	else
		while (tmp.y++ < p2.y)
			mlx_pixel_put(env.mlx, env.win, tmp.x, tmp.y, tmp.color);
}

int		calculate_color(float h, int max)
{
	int		r;
	int		g;
	int		b;
	float	diff;
	int		color_max;

	color_max = 0x7C269F;
	diff = h / max;
	if (diff < 0)
		diff = (-1 * diff);
	if (h < 0)
		color_max = 0x00CDCD;
	r = (((color_max / (256 * 256)) - 255) * diff) + 255;
	g = ((((color_max / 256) % 256) - 255) * diff) + 255;
	b = (((color_max % 256) - 255) * diff) + 255;
	return ((r * 256 * 256) + (g * 256) + b);
}

int		key_hook(int keycode, t_env *e)
{
	e = NULL;
	if (keycode == 53)
		exit(0);
	return (0);
}

int		main(int argc, char **argv)
{
	t_env e;

	if (argc == 2)
	{
		e.mlx = mlx_init();
		e.win = mlx_new_window(e.mlx, WIDTH, HEIGHT, "42");
		mlx_key_hook(e.win, key_hook, &e);
		print_isometric(e, argv[1], get_max_value(argv[1]));
		mlx_loop(e.mlx);
	}
	else
		ft_putstr_fd("You need to provide a filepath\n", 2);
	return (0);
}
