/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   geometry.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aacuna <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/17 12:06:50 by aacuna            #+#    #+#             */
/*   Updated: 2015/12/17 15:08:49 by aacuna           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

double	to_radians(int x)
{
	return ((x * M_PI) / 180);
}

int		isometric_x(t_point p, int offset, float size)
{
	int	horizontal;

	horizontal = 0;
	horizontal = (p.y * size * cos(to_radians(-30)));
	return (offset - horizontal + (p.x * size * cos(to_radians(30))));
}

int		isometric_y(t_point p, int offset, float size, int height)
{
	int horizontal;

	horizontal = 0;
	horizontal = (p.y * size * sin(to_radians(-30)));
	return (offset - horizontal + (p.x * size * sin(to_radians(30))) - height);
}
